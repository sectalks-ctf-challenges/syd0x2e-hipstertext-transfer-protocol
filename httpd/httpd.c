#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h> 
#include <unistd.h>

// Compile command:
// gcc -o httpd -m32 httpd.c

#define PORT 8080		// the port users will be connecting to
#define BUFSIZE 1024	// size of the recv buffer

// display an error message and then exit
void fatal(char *message) {
	char error_message[100];

	strcpy(error_message, "[!!] Fatal Error ");
	strncat(error_message, message, 83);
	perror(error_message);
	exit(-1);
}

// dumps raw memory in hex byte and printable split format
void dump(const unsigned char *data_buffer, const unsigned int length) {
	unsigned char byte;
	unsigned int i, j;
	for(i=0; i < length; i++) {
		byte = data_buffer[i];
		printf("%02x ", data_buffer[i]);  // display byte in hex
		if(((i%16)==15) || (i==length-1)) {
			for(j=0; j < 15-(i%16); j++)
				printf("   ");
			printf("| ");
			for(j=(i-(i%16)); j <= i; j++) {  // display printable bytes from line
				byte = data_buffer[j];
				if((byte > 31) && (byte < 127)) // outside printable char range
					printf("%c", byte);
				else
					printf(".");
			}
			printf("\n"); // end of the dump line (each line 16 bytes)
		}
	}
}

// close connection and exit (child) process
void close_socket(int new_sockfd){
	// close file descriptor
	shutdown(new_sockfd, SHUT_RDWR);
	close(new_sockfd);
}

// send a generic response
// Note: headers and body are assumed to end in CRLF (unless they are empty)
void send_response(int new_sockfd, int status_code, char* reason_phrase,  char* headers, char* body, int HEAD){
	
	// HTTP1.1 <status_code> <reaon_phrase>CRLF
	// Server: My First HTTP ServerCRLF
	// <headers>
	// Content-Length: <len(body)>
	// Connection: close

	// CRLF
	// <body>

	char* server = "My First HTTP server";
	FILE *fd = fdopen(new_sockfd, "w");

	// if method was HEAD, don't return body and Contect-Length header
	if (HEAD){
		fprintf(fd, 
			"HTTP/1.1 %d %s\x0d\x0a"
			"Server: %s\x0d\x0a"
			"%s"
			"Connection: close\x0d\x0a"
			"Content-Type: text/html; charset=iso-8859-1\x0d\x0a"
			"\r\n"
			, status_code, reason_phrase, server, headers);
	}
	else {
		fprintf(fd, 
		"HTTP/1.1 %d %s\x0d\x0a"
		"Server: %s\x0d\x0a"
		"%s"
		"Content-Length: %d\x0d\x0a"
		"Connection: close\x0d\x0a"
		"Content-Type: text/html; charset=iso-8859-1\x0d\x0a"
		"\x0d\x0a"
		"%s"
		, status_code, reason_phrase, server, headers, strlen(body), body);
	}

	fflush(fd);
	exit(0);	// kill child process
}

// send 400 Bad Request
void bad_request(int new_sockfd, int HEAD){

	// HTTP/1.1 400 Bad Request
	// Server: <server>
	// Content-Length: <len(body)>
	// Connection: close

	// <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
	// <html><head>
	// <title>400 Bad Request</title>
	// </head><body>
	// <h1>Bad Request</h1>
	// <p>WTF, bro? No comprende.<br/></p>
	// </body></html>

	send_response(new_sockfd, 400, "Bad Request", "", 
		"<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\x0d\x0a"
		"<html><head>\x0d\x0a"
		"<title>400 Bad Request</title>\x0d\x0a"
		"</head><body>\x0d\x0a"
		"<h1>Bad Request</h1>\x0d\x0a"
		"<p>WTF, bro? No comprende.<br/></p>\x0d\x0a"
		"</body></html>\x0d\x0a"
		, HEAD);
}

// send 501 Not Implemented
void not_implemented(int new_sockfd, int HEAD){

	// HTTP/1.1 501 Not Implemented
	// Server: <server>
	// Content-Length: <len(body)>
	// Connection: close

	// <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
	// <html><head>
	// <title>501 Not Implemented</title>
	// </head><body>
	// <h1>Bad Request</h1>
	// <p>David was too lazy to implement this method. Sorry.<br/></p>
	// </body></html>

	send_response(new_sockfd, 501, "Not Implemented", "", 
		"<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\x0d\x0a"
		"<html><head>\x0d\x0a"
		"<title>501 Not Implemented</title>\x0d\x0a"
		"</head><body>\x0d\x0a"
		"<h1>Not Implemented</h1>\x0d\x0a"
		"<p>David was too lazy to implement this method. Sorry.<br/></p>\x0d\x0a"
		"</body></html>\x0d\x0a"
		, HEAD);
}

void ok(int new_sockfd, int HEAD){

	// HTTP/1.1 200 OK
	// Date: Sat, 18 May 2019 06:04:33 GMT
	// Server: Apache/2.4.38 (Debian)
	// Last-Modified: Wed, 30 Jan 2019 07:12:29 GMT
	// #ETag: "29cd-580a7a1fa9140"
	// #Accept-Ranges: bytes
	// Content-Length: 10701
	// #Vary: Accept-Encoding
	// Connection: close
	// Content-Type: text/html

	// <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
	// <html><head>
	// <title>Welcome</title>
	// </head><body>
	// <h1>New server, who dis?</h1>
	// <p>Hey, it works.<br/></p>
	// </body></html>

	send_response(new_sockfd, 200, "OK", "Content-Type: text/html\x0d\x0a", 
		"<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\x0d\x0a"
		"<html><head>\x0d\x0a"
		"<title>Welcome</title>\x0d\x0a"
		"</head><body>\x0d\x0a"
		"<h1>New server, who dis?</h1>\x0d\x0a"
		"<p>Hey, it works.<br/></p>\x0d\x0a"
		"</body></html>\x0d\x0a"
		, HEAD);

}

// receieve HTTP message
int read_HTTP_message(int new_sockfd, char* buffer){
	char* append = buffer;				// pointer to append to buffer
	int remaining = BUFSIZE;			// space left in buffer
	int recv_length = 1;				// length of incoming data

	// fill buffer with NULLs
	memset(buffer, '\0', BUFSIZE);

	// read until 2 CRLFs are detected
	recv_length = recv(new_sockfd, buffer, BUFSIZE, 0);
	append += recv_length;
	remaining -= recv_length;
	dump(buffer, BUFSIZE - remaining);
	printf("Bytes read: %d\n", recv_length);
	while(recv_length > 0){
		// if 2 CRLFs, stop listening
		if (strstr(buffer, "\x0d\x0a\x0d\x0a") != 0){
			puts("Found 2 CRLFs");
			return 0;
		}
		puts("No double CRLF so far; keep listenig.");

		// check if buffer is full
		if (remaining == 0){
			puts("Overflow detected.");
			return 1;
		}
		recv_length = recv(new_sockfd, append, BUFSIZE - remaining, 0);
		append += recv_length;
		remaining -= recv_length;
		dump(buffer, BUFSIZE - remaining);
		printf("Bytes read: %d\n", recv_length);
		printf("remaining: %d\n", remaining);
	}
	puts("Client closed connection.");
	return 1;
}

// handle incoming message
void handle_incoming(int new_sockfd, struct sockaddr_in client_addr){
	int HEAD = 0;						// is HEAD the method being used?
	int err = 0;						// error encountered when reading data?
	char buffer[BUFSIZE];				// buffer for incoming data
	char *first_space, *second_space;	// locations of the spaces in the request line
	

	// log connection
	printf("server: got connection from %s port %d\n",inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

	// read data until 2 CRLFs
	if (read_HTTP_message(new_sockfd, buffer)){
		// Something went wrong
		return;
	}

	// parse HTTP message
	// parse request line = method SP request-target SP HTTP-version CRLF

	// if there is no space in the request line, send 400
	if (!(first_space = strchr(buffer, ' '))){
		puts("No first space.");
		bad_request(new_sockfd, HEAD);
		return;
	}

	// check HTTP method
	// If the method is HEAD, set the HEAD flag.
	// Otherwise, it has to be GET; if it's not, send 501.
	if(strncmp(buffer, "HEAD ", 5) == 0){
		puts("HEAD detected.");
		HEAD = 1;
	} else if (strncmp(buffer, "GET ", 4) != 0){
		puts("Unknown method.");
		not_implemented(new_sockfd, HEAD);
		return;
	}
	puts("Method recognised.");

	// if there isn't an second space, send 400
	if (!(second_space = strchr(first_space+1, ' '))){
		puts("No second space.");
		bad_request(new_sockfd, HEAD);
		return;
	}

	// check HTTP version
	if ( (strncmp(second_space+1, "HTTP/1.1\x0d\x0a", 10) != 0) && (strncmp(second_space+1, "HTTP/1.0\x0d\x0a", 10) != 0)){
		puts("Unknown HTTP version.");
		bad_request(new_sockfd, HEAD);
		return;
	}

	// request line is ok
	puts("Request line parsed.");

	// Send HTML page
	ok(new_sockfd, HEAD);
	
}

int main(void) {
	int sockfd;						// file descriptor of listening socket
	int new_sockfd;					// file descriptor of new connection
	struct sockaddr_in host_addr;	// address information of server
	struct sockaddr_in client_addr;	// address information of client
	socklen_t sin_size;				// stores the size of the incoming address structure
	int yes = 1;					// true	(used to set socket as reussable)
	
	// create socket
	// PF_INET is for IPv4
	// SOCK_STREAM is for a stream socket
	if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		fatal("in socket");

	// make socket reuseable
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
		fatal("setting socket option SO_REUSEADDR");
	
	// define values needed to bind socket
	host_addr.sin_family = AF_INET;			// host byte order
	host_addr.sin_port = htons(PORT);		// short, network byte order
	host_addr.sin_addr.s_addr = INADDR_ANY;	// automatically fill with my IP
	memset(&(host_addr.sin_zero), '\0', 8);	// zero the rest of the struct

	// bind socket
	if (bind(sockfd, (struct sockaddr *)&host_addr, sizeof(struct sockaddr)) == -1)
		fatal("binding to socket");

	// start listening
	if (listen(sockfd, 5) == -1)
		fatal("listening on socket");

	// accept loop
	while(1) {
		// receive connection    
		sin_size = sizeof(struct sockaddr_in);
		new_sockfd = accept(sockfd, (struct sockaddr *)&client_addr, &sin_size);
		
		// check that file descriptor is OK
		if(new_sockfd == -1)
			fatal("accepting connection");
		
		// if OK, handle connection in child process
		else {
			if(fork() == 0){
				// parse 1 HTTP message
				handle_incoming(new_sockfd, client_addr);
				puts("Back in main");
				close_socket(new_sockfd);	// technically not necessary, but just in case
			}

		}
	}

	return 0;
}

